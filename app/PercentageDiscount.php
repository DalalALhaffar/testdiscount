<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PercentageDiscount extends Model
{
    protected $hidden=[ "product_id",
    "created_at",
    "updated_at"];
    //
    public function discount()
    {
        return $this->belongsTo(Discount::class);
    }
}
