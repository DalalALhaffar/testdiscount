<?php

namespace App\Http\Controllers\api;

use App\FixedDiscount;
use App\Http\Controllers\Controller;
use App\Http\Resources\ProductCollection;
use App\Product;
use Illuminate\Auth\Events\Failed;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class ProductController extends Controller
{
    //
    public function index(Request $request)
    {
        if(!Auth::check())
        {
return response()->json(['status'=>'Failed','message'=>'Unauthenticated'],401);
        }
        $products = Product::where('id', '!=', '0');
        if (isset($request['term'])) {
            $term_pieces = explode(" ", $request['term']);
            $termsimploaded = implode("|", $term_pieces);
            $products == $products->where('name', 'Rlike', $termsimploaded);
        }
        if ($request->has('filter')) {
            switch ($request['filter']) {
                case 'recent':
                    $products->orderby('created_at', 'DESC');
                    break;
                case 'alpha':
                    $products->orderby('name', 'ASC');
                    break;
            }
        }
        return response()->json(['status' => 'success', 'data' => new ProductCollection($products->get())], 200);
    }
    private function ValidationRules()
    {
        return [
            'product_id' => 'required|exists: products,id',
            'quantity' => 'required|numeric|gt:products,quantity',
        ];
    }

    public function calculate_total(Request $request)
    {

        if(!Auth::check())
        {
return response()->json(['status'=>'Failed','message'=>'Unauthenticated'],401);
        }
        $final_cost = 0;
        $total_cost = 0;
        $cart_numb = 0;
        $got_product_discount = false;
        $got_fixed_discount = false;
        $products = json_decode($request['products']);
        foreach ($products as $productQuantity) {
            $product = Product::find($productQuantity->product_id);
            $total_cost += $product->price * $productQuantity->quantity;
            $cart_numb += $productQuantity->quantity;
        }

        $finalp = collect(json_decode($request['products']));
        foreach ($products as $productQuantity) {
            $product = Product::find($productQuantity->product_id);
            foreach ($product->buyx_gety as $xydiscount) {
                if ($this->ValidCoupon($xydiscount, $cart_numb, $total_cost) && $xydiscount->quantity <= $productQuantity->quantity) {
                    $x = $finalp->where('product_id', $xydiscount->productsy_id)->first();
                    if (isset($x)) {
                        $val = (int)$x->quantity;
                        $x->quantity = $x->quantity > 0 ?
                            ($val - (int)($val / $xydiscount->quantity) > 0 ? $val - (int)($productQuantity->quantity / $xydiscount->quantity) : 0) : 0;
                    }
                }
            }
        }

        foreach ($finalp as $productQuantity) {
            $product = Product::find($productQuantity->product_id);
            $total_cost = $product->price * $productQuantity->quantity;
            foreach ($product->percentage_discounts as $pdiscount) {
                if (!$got_product_discount) {
                    $total_cost = $this->ValidCoupon($pdiscount, $cart_numb, $total_cost) ? $total_cost - ($total_cost * $pdiscount->amount / 100) : $total_cost;
                    $got_product_discount = true;
                }
            }
            $got_product_discount = false;


            $final_cost += $total_cost;
        }
        $fixed_discounts = valid_discount(FixedDiscount::where('id', '!=', '0'))->get();
        foreach ($fixed_discounts as $fdiscount) {
if(!$got_fixed_discount){
    $final_cost = $this->ValidCoupon($fdiscount, $cart_numb, $total_cost) ? ($final_cost -$fdiscount->amount  > 0 ? $final_cost - $fdiscount->amount : 0) : $final_cost;
    $got_fixed_discount=true;
}
        }

        return response()->json(['status' => 'success', 'total_cost' => $final_cost], 200);

    }

    public function ValidCoupon($coupon, $items, $total)
    {
        if (!($coupon->lowest_paid) && $coupon->lowest_paid <= $total)
            return true;
        if (!($coupon->minimum_number_cart) && $coupon->minimum_number_cart <= $items)
            return true;

        return false;
    }
}
