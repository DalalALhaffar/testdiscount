<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    public function percentage_discounts()
    {
        return valid_discount($this->hasMany(PercentageDiscount::class));
    }
    public function buyx_gety()
    {
        return valid_discount($this->hasMany(BuyxGetyDiscount::class));
    }

}
