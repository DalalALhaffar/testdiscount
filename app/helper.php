<?php

use App\Discount;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

function valid_discount($data)
{
    $unUsedCoupons=[];
    $used_coupons=Auth::user()->discounts->pluck('id')->toArray();
    $unUsedCoupons= Discount::where('only_once','0')->orwhere(function($query) use ($used_coupons){
     $query-> where('only_once','1')->whereNotIn('id',$used_coupons);
    })->pluck('id')->toArray();
         return $data->wherehas('discount', function($q)use($unUsedCoupons){
        $q->where('end','>',Carbon::now())->where('start','<',Carbon::now())
        ->whereIn('id',$unUsedCoupons)
        ->where(function ($query) {
            $query->where('max_number_users', '>', '0')
                ->orWhereNull('max_number_users');
        })
        ;
    })->with('discount');
}
