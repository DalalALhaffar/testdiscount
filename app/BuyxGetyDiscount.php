<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BuyxGetyDiscount extends Model
{
    //
    public function discount()
    {
        return $this->belongsTo(Discount::class);
    }
}
