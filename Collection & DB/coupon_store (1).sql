-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 24, 2021 at 12:16 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `coupon_store`
--

-- --------------------------------------------------------

--
-- Table structure for table `buyx_gety_discounts`
--

CREATE TABLE `buyx_gety_discounts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `discount_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `productsy_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `buyx_gety_discounts`
--

INSERT INTO `buyx_gety_discounts` (`id`, `discount_id`, `product_id`, `productsy_id`, `created_at`, `updated_at`, `quantity`) VALUES
(1, 3, 5, 9, '2021-03-22 20:51:08', NULL, 2),
(2, 4, 6, 7, '2021-03-22 20:51:08', NULL, 3);

-- --------------------------------------------------------

--
-- Table structure for table `discounts`
--

CREATE TABLE `discounts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `start` timestamp NOT NULL DEFAULT current_timestamp(),
  `end` timestamp NULL DEFAULT NULL,
  `max_number_users` int(11) DEFAULT NULL,
  `lowest_paid` decimal(8,2) DEFAULT NULL,
  `minimum_number_cart` int(11) DEFAULT NULL,
  `only_once` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('buyXgetY','fixed','percentage') COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `discounts`
--

INSERT INTO `discounts` (`id`, `start`, `end`, `max_number_users`, `lowest_paid`, `minimum_number_cart`, `only_once`, `status`, `created_at`, `updated_at`, `title`, `type`) VALUES
(1, '2021-03-22 19:04:08', '2021-03-29 18:03:34', 10, '2000.00', NULL, '1', 'active', '2021-03-22 19:03:34', NULL, 'Happy mothers day', 'fixed'),
(2, '2021-03-22 19:04:08', '2021-03-25 21:27:09', NULL, '2000.00', NULL, '1', 'active', '2021-03-22 19:03:34', NULL, 'Annual coupon', 'percentage'),
(3, '2021-03-22 19:04:08', '2021-03-25 19:03:34', NULL, NULL, 2, '0', 'active', '2021-03-22 19:03:34', NULL, 'buy two get y free', 'buyXgetY'),
(4, '2021-03-22 19:04:08', '2021-03-24 19:03:34', NULL, NULL, 2, '0', 'active', '2021-03-22 19:03:34', NULL, 'buy two get y free', 'buyXgetY');

-- --------------------------------------------------------

--
-- Table structure for table `discount_user`
--

CREATE TABLE `discount_user` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `discount_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `discount_user`
--

INSERT INTO `discount_user` (`id`, `user_id`, `discount_id`, `created_at`, `updated_at`) VALUES
(1, 5, 1, NULL, NULL),
(2, 6, 2, NULL, NULL),
(4, 1, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fixed_discounts`
--

CREATE TABLE `fixed_discounts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `discount_id` bigint(20) UNSIGNED NOT NULL,
  `amount` decimal(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fixed_discounts`
--

INSERT INTO `fixed_discounts` (`id`, `discount_id`, `amount`, `created_at`, `updated_at`) VALUES
(1, 1, '1000.00', '2021-03-22 19:04:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(5, '2021_03_22_181142_create_products_table', 2),
(6, '2021_03_22_181152_create_discounts_table', 2),
(7, '2021_03_22_181213_create_fixed_discounts_table', 2),
(8, '2021_03_22_181247_create_percentage_discounts_table', 2),
(9, '2021_03_22_181311_create_buyx_gety_discounts_table', 2),
(10, '2021_03_23_191911_add_quantity_to_products_table', 3),
(11, '2021_03_23_192052_add_quantity_to_buyxgety_table', 4),
(13, '2021_03_23_212528_create_discount_users_table', 5),
(14, '2016_06_01_000001_create_oauth_auth_codes_table', 6),
(15, '2016_06_01_000002_create_oauth_access_tokens_table', 6),
(16, '2016_06_01_000003_create_oauth_refresh_tokens_table', 6),
(17, '2016_06_01_000004_create_oauth_clients_table', 6),
(18, '2016_06_01_000005_create_oauth_personal_access_clients_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('03481a6a6fcdacb1dd3774b2a74b1808ea3531562e3f7fee7f50645a0d313310dee618e9f5b65ade', 1, 3, 'myApp', '[]', 0, '2021-03-23 21:09:04', '2021-03-23 21:09:04', '2022-03-23 23:09:04'),
('2b6822dcbf68b7e355521230035a926548d7346994734954aeb7318eaf1aaadbf281f0dd6b6bb535', 1, 3, 'myApp', '[]', 0, '2021-03-23 20:22:36', '2021-03-23 20:22:36', '2022-03-23 22:22:36'),
('75ddaef338a37a8aeed0533e3d279740ff1683ee3f94b76e502c9584f76bd7c21203ba8faa312dba', 1, 3, 'myApp', '[]', 0, '2021-03-23 21:10:03', '2021-03-23 21:10:03', '2022-03-23 23:10:03'),
('f161c31ab91202a57354029eaa224315530a58c15a1cd6ad9dbf1375021ac7168ac032ed91405c81', 2, 3, 'myApp', '[]', 0, '2021-03-23 21:09:19', '2021-03-23 21:09:19', '2022-03-23 23:09:19');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'Lu9Ve1aSRDPzbtlFRmbaHRRwQ3hloZlzEwoBeJ8c', NULL, 'http://localhost', 1, 0, 0, '2021-03-23 20:12:39', '2021-03-23 20:12:39'),
(2, NULL, 'Laravel Password Grant Client', '5lC8zRfcpxwbaybhBKD9Bz7AMExVu69xhQpEvzHx', 'users', 'http://localhost', 0, 1, 0, '2021-03-23 20:12:40', '2021-03-23 20:12:40'),
(3, NULL, 'test', 'FEZTfYSMhT7h4aO7BUENhYDw0Du3tUA09LCFtX0V', NULL, 'http://localhost', 1, 0, 0, '2021-03-23 20:20:21', '2021-03-23 20:20:21');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2021-03-23 20:12:40', '2021-03-23 20:12:40'),
(2, 3, '2021-03-23 20:20:21', '2021-03-23 20:20:21');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `percentage_discounts`
--

CREATE TABLE `percentage_discounts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `discount_id` bigint(20) UNSIGNED NOT NULL,
  `amount` int(11) NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `percentage_discounts`
--

INSERT INTO `percentage_discounts` (`id`, `discount_id`, `amount`, `product_id`, `created_at`, `updated_at`) VALUES
(1, 2, 20, 1, '2021-03-22 19:05:46', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `description`, `price`, `created_at`, `updated_at`, `quantity`) VALUES
(1, 'Herminio Grant', 'Sit et eos veniam quia pariatur. Aperiam possimus accusantium quis dignissimos laborum sint. Eos molestiae eius asperiores sed delectus aut veritatis.', '100.00', '2021-03-23 16:59:46', '2021-03-22 16:59:46', 10),
(2, 'Herminio Gulgowski', 'Nostrum iure ut accusantium.Bradly Rohan ugiat eum. Inventore modi sunt non pariatur earum magni.', '33918.00', '2021-03-22 16:59:46', '2021-03-22 16:59:46', 10),
(3, 'Jamarcus Walker', 'Possimus quia sed cum illum voluptatem non. Omnis pariatur autem vel nesciunt maxime eaque corrupti voluptas. Excepturi nihil maxime reprehenderit ipsa facilis beatae.', '16272.00', '2021-03-22 16:59:46', '2021-03-22 16:59:46', 10),
(4, 'Zaria Mueller', 'Consequuntur quaerat aut nam rerum et laborum cupiditate. Aliquid facere modi sed consequatur. Ut iusto quod consequatur quia molestiae placeat ut.', '50000.00', '2021-03-22 16:59:47', '2021-03-22 16:59:47', 10),
(5, 'Bradly Rohan', 'Mollitia voluptas molestias sunt cupiditate nisi corporis. Blanditiis quia qui earum illo ipsum. Qui itaque fugit minima est et sunt et. Rerum deserunt rem tempore magnam.', '500.00', '2021-03-22 16:59:47', '2021-03-22 16:59:47', 10),
(6, 'Jerald Roob', 'Quam odit itaque explicabo totam illum. Minima explicabo consequatur ex quia non non quisquam. Soluta debitis corrupti similique id occaecati dolores sit. A voluptatem eum ipsam similique et.', '6000.00', '2021-03-22 16:59:47', '2021-03-22 16:59:47', 10),
(7, 'Zetta Hauck', 'Molestias beatae hic earum vero quaerat. Hic esse pariatur illo dolor. Eveniet ducimus accusantium quibusdam numquam similique magnam ipsa in.', '20810.00', '2021-03-22 16:59:47', '2021-03-22 16:59:47', 10),
(8, 'Daryl Trantow', 'Velit in voluptatem rerum similique. Officia nam quia odio quidem quia aliquid. Dolores quisquam quo alias qui veritatis accusantium. Ipsum qui quos error qui.', '29041.00', '2021-03-22 16:59:47', '2021-03-22 16:59:47', 10),
(9, 'Kaelyn Wiza', 'Labore explicabo omnis assumenda ut. Nobis dolorem tempore quis blanditiis recusandae. Facilis quo incidunt quidem dicta ut.', '900.00', '2021-03-22 16:59:47', '2021-03-22 16:59:47', 10),
(10, 'Ola Mills DVM', 'Architecto accusantium ut voluptatibus consequatur. Molestiae laudantium ut voluptas ipsam. Non maiores quas dignissimos velit sint dolorem.', '58722.00', '2021-03-22 16:59:47', '2021-03-22 16:59:47', 10);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Cristina Torphy', 'sauer.dejon@example.org', '2021-03-22 17:02:53', '$2y$10$RUIf1FYcgLW6khJX4QGinu0g5S/HRvXP725ZJHcLF88IFeIHbshgG', 'u6itXzagSv', '2021-03-22 17:02:53', '2021-03-22 17:02:53'),
(2, 'Bethel Krajcik', 'rmcglynn@example.net', '2021-03-22 17:02:53', '$2y$10$RUIf1FYcgLW6khJX4QGinu0g5S/HRvXP725ZJHcLF88IFeIHbshgG', 'T6AFcglAnw', '2021-03-22 17:02:53', '2021-03-22 17:02:53'),
(3, 'Rosalia Purdy', 'maegan20@example.com', '2021-03-22 17:02:53', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '3nzhsPPkSi', '2021-03-22 17:02:53', '2021-03-22 17:02:53'),
(4, 'Alessia Goodwin', 'kstracke@example.org', '2021-03-22 17:02:53', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '9reAIJK7nm', '2021-03-22 17:02:53', '2021-03-22 17:02:53'),
(5, 'Mrs. Lou Blick', 'green.giovani@example.net', '2021-03-22 17:02:53', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'sFpRSnbTiZ', '2021-03-22 17:02:53', '2021-03-22 17:02:53'),
(6, 'Newton Legros', 'ejacobson@example.org', '2021-03-22 17:02:53', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'uTdM49iTF1', '2021-03-22 17:02:54', '2021-03-22 17:02:54'),
(7, 'Annabell Powlowski', 'bettie.moen@example.com', '2021-03-22 17:02:53', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '5zEccG3xTy', '2021-03-22 17:02:54', '2021-03-22 17:02:54'),
(8, 'Johnathan Russel', 'rogahn.elijah@example.com', '2021-03-22 17:02:53', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'bfS1Y9dSJh', '2021-03-22 17:02:54', '2021-03-22 17:02:54'),
(9, 'Juliet Lueilwitz Jr.', 'carolyn42@example.org', '2021-03-22 17:02:53', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'BtKbE1Bfae', '2021-03-22 17:02:54', '2021-03-22 17:02:54'),
(10, 'Ms. Golda Rohan', 'dooley.sigrid@example.com', '2021-03-22 17:02:53', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '3o7gIEfbC6', '2021-03-22 17:02:54', '2021-03-22 17:02:54');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `buyx_gety_discounts`
--
ALTER TABLE `buyx_gety_discounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `discounts`
--
ALTER TABLE `discounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `discount_user`
--
ALTER TABLE `discount_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fixed_discounts`
--
ALTER TABLE `fixed_discounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `percentage_discounts`
--
ALTER TABLE `percentage_discounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `buyx_gety_discounts`
--
ALTER TABLE `buyx_gety_discounts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `discounts`
--
ALTER TABLE `discounts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `discount_user`
--
ALTER TABLE `discount_user`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fixed_discounts`
--
ALTER TABLE `fixed_discounts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `percentage_discounts`
--
ALTER TABLE `percentage_discounts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
