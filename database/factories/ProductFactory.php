<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name'=>$faker->name,
        'description'=> $faker->paragraph,
        'price'=>$faker->numberBetween($min = 1000, $max = 60000),
        'quantity'=>$faker->numberBetween($min = 10, $max = 100),
    ];
});

