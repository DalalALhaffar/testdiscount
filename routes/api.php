<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('login', 'api\AuthController@login');
Route::group(['namespace' => 'api'], function () {
        Route::group(['middleware' => 'auth:api'], function () {
Route::post('/products','ProductController@index');
Route::post('/total','ProductController@calculate_total');

});
});
